#!/bin/bash

echo "Criando serviços no cluster Kubernetes"


echo "Criando Namespace devops"
kubectl create namespace devops

echo "Criar Serviço Volumes Persistentes para Aplicação"
kubectl apply -f k8s/services/pv-dados-nfs.yaml -n devops

echo "Criar Serviço Redimensionamento de Volumes Persistentes para o MySql"
kubectl apply -f k8s/services/pvc-mysql.yaml -n devops

echo "Criar Serviço Redimensionamento de Volumes Persistentes para Aplicação"
kubectl apply -f k8s/services/pvc-dados-nfs.yaml -n devops

echo "Criar Serviço do MySql"
kubectl apply -f k8s/services/service-mysql.yaml -n devops

echo "Criar Serviço de LoadBalancer para o PHP"
kubectl create -f k8s/services/loadBalancer-php.yaml -n devops
