#!/bin/bash
echo "Deletando APP e o BD"
kubectl delete -f k8s/app-bd/ -n devops

echo "Deletando todos os Servicos provisionados"
kubectl delete -f k8s/services/ -n devops
