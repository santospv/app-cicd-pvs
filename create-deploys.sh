#!/bin/bash

echo "Criando deploys no cluster Kubernets"

echo "Realizar Deploy do Mysql"
kubectl apply -f k8s/app-bd/deployment-mysql.yaml -n devops --record

echo "Realizar Deploy da aplicação PHP"
kubectl apply -f k8s/app-bd/deployment-php.yaml -n devops --record
